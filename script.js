let tabsTitle = document.querySelector(".tabs");
let tabsContent = document.querySelector(".tabs-content");
console.log(tabsTitle);

tabsTitle.addEventListener("click", (event) => {
  tabsTitle.querySelector(".active")?.classList.toggle("active");
  let target = event.target;

  if (target.closest("li")) {
    target.classList.toggle("active");
  }

  const tabId = target.dataset.tab;
  console.log(tabId);
  tabsContent
    .querySelector(".content-active")
    ?.classList.toggle("content-active");
  tabsContent
    .querySelector(`.content[data-tab="${tabId}"]`)
    ?.classList.toggle("content-active");
});

